import { DeepPartial, EntityManager, getManager, FindOneOptions, FindManyOptions } from "typeorm";
import { QueryDeepPartialEntity } from "typeorm/query-builder/QueryPartialEntity";
import { Subscription } from "../entities/subscription";


export class SubscriptionRepository {

    public static async insert(model: DeepPartial<Subscription>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).save(Subscription, { ...model });
    }

    public static async findOne(options: FindOneOptions<Subscription>, entityManager?: EntityManager): Promise<Subscription | undefined> {
        return await (entityManager || getManager()).findOne(Subscription, options);
    }

    public static async findOneOrFail(options: FindOneOptions<Subscription>, entityManager?: EntityManager): Promise<Subscription> {
        return await (entityManager || getManager()).findOneOrFail(Subscription, options);
    }

    public static async find(options: FindManyOptions<Subscription>, entityManager?: EntityManager): Promise<Subscription[]> {
        return await (entityManager || getManager()).find(Subscription, options);
    }

    public static async update(criteria: any, partialEntity: QueryDeepPartialEntity<Subscription>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).update(Subscription, criteria, partialEntity);
    }

    public static async delete(criteria: any, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).delete(Subscription, criteria);
    }
}
