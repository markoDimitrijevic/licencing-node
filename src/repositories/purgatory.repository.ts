import { DeepPartial, EntityManager, FindManyOptions, FindOneOptions, getManager } from "typeorm";
import { Purgatory } from "../entities/purgatory";


export class PurgatoryRepository {

    public static async insert(model: DeepPartial<Purgatory>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).save(Purgatory, { ...model });
    }

    public static async find(options: FindManyOptions<Purgatory>, entityManager?: EntityManager): Promise<Purgatory[]> {
        return await (entityManager || getManager()).find(Purgatory, options);
    }

    public static async findOne(options: FindOneOptions<Purgatory>, entityManager?: EntityManager): Promise<Purgatory | undefined> {
        return await (entityManager || getManager()).findOne(Purgatory, options);
    }

    public static async delete(criteria: any, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).delete(Purgatory, criteria);
    }

}