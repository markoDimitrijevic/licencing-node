import { DeepPartial, EntityManager, getManager, FindOneOptions, FindManyOptions } from "typeorm";
import { QueryDeepPartialEntity } from "typeorm/query-builder/QueryPartialEntity";
import { Business } from "../entities/business";

export class BusinessRepository {

    public static async insert(model: DeepPartial<Business>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).save(Business, { ...model });
    }

    public static async findOne(options: FindOneOptions<Business>, entityManager?: EntityManager): Promise<Business | undefined> {
        return await (entityManager || getManager()).findOne(Business, options);
    }

    public static async find(options: FindManyOptions<Business>, entityManager?: EntityManager): Promise<Business[]> {
        return await (entityManager || getManager()).find(Business, options);
    }

    public static async findOneOrFail(options: FindOneOptions<Business>, entityManager?: EntityManager): Promise<Business> {
        return await (entityManager || getManager()).findOneOrFail(Business, options);
    }

    public static async update(criteria: any, partialEntity: QueryDeepPartialEntity<Business>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).update(Business, criteria, partialEntity);
    }

    public static async delete(criteria: any, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).delete(Business, criteria);
    }
}
