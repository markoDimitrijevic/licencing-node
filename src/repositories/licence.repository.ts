import { DeepPartial, EntityManager, FindManyOptions, FindOneOptions, getManager } from "typeorm";
import { QueryDeepPartialEntity } from "typeorm/query-builder/QueryPartialEntity";
import { Licence } from "../entities/licence";

export class LicenceRepository {

    public static async insert(model: DeepPartial<Licence>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).save(Licence, { ...model });
    }

    public static async findOne(options: FindOneOptions<Licence>, entityManager?: EntityManager): Promise<Licence | undefined> {
        return await (entityManager || getManager()).findOne(Licence, options);
    }

    public static async findOneOrFail(options: FindOneOptions<Licence>, entityManager?: EntityManager): Promise<Licence> {
        return await (entityManager || getManager()).findOneOrFail(Licence, options);
    }

    public static async find(options: FindManyOptions<Licence>, entityManager?: EntityManager): Promise<Licence[]> {
        return await (entityManager || getManager()).find(Licence, options);
    }

    public static async update(criteria: any, partialEntity: QueryDeepPartialEntity<Licence>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).update(Licence, criteria, partialEntity);
    }

    public static async delete(criteria: any, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).delete(Licence, criteria);
    }
}
