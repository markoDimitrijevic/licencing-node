import { DeepPartial, EntityManager, getManager, FindOneOptions, FindManyOptions } from "typeorm";
import { QueryDeepPartialEntity } from "typeorm/query-builder/QueryPartialEntity";
import { Activity } from "../entities/activity";

export class ActivityRepository {

    public static async insert(model: DeepPartial<Activity>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).save(Activity, { ...model });
    }

    public static async findOne(options: FindOneOptions<Activity>, entityManager?: EntityManager): Promise<Activity | undefined> {
        return await (entityManager || getManager()).findOne(Activity, options);
    }

    public static async find(options: FindManyOptions<Activity>, entityManager?: EntityManager): Promise<Activity[]> {
        return await (entityManager || getManager()).find(Activity, options);
    }

    public static async update(criteria: any, partialEntity: QueryDeepPartialEntity<Activity>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).update(Activity, criteria, partialEntity);
    }

    public static async delete(criteria: any, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).delete(Activity, criteria);
    }
}
