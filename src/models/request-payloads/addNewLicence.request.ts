import { LicenceType } from "../enums/licence-type.enum";

export interface AddNewLicenceRequestModel { subscriptionId: number, type: LicenceType, startDate?: Date }