export interface UpdateActivityRequestModel {
    customerId: string,
    sessionId: string
}