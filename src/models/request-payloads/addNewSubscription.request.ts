import { ActivityType } from "../enums/activity-type.enum";
import { SubscriptionType } from "../enums/subscription-type.enum";

export interface AddNewSubscriptionRequestModel {
    businessId: number;
    activationDate: Date,
    expirationDate: Date,
    type: SubscriptionType,
    usagePeriod: number,
    allowedActivities: ActivityType[],
    maxNumberOfAllowedActivities: number
    name: string
}

