export interface ActivateSubscriptionRequestModel {
    subscriptionId: number;
    businessId: number;
}