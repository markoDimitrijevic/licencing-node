export interface ActivateLicenceRequestModel {
    licenceId: number;
    subscriptionId: number;
}