
export enum ActivityType {
    ADD_NEW_CLIENT = "addNewClient",
    UPDATE_CUSTOMER = "updateCustomer",
    ADMIN_ADD_NEW_CLIENT = "admin/addNewClient",
    REPEAT_SESSION = "repeatSession"
}