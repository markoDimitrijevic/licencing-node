export const allowedSubscriptionStatuses = ["Active", "Pending", "Created"];

export enum SubscriptionStatus {
    ACTIVE = "Active",
    PENDING = "Pending",
    EXPIRED = "Expired",
    LOCKED = "Locked",
    CREATED = "Created", // default value
    DEACTIVATED = "Deactivated"
}