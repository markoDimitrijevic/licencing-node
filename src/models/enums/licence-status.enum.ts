export const allowedLicenceStatuses = ["Active", "Pending", "Created"];

export enum LicenceStatus {
    ACTIVE = "Active",
    PENDING = "Pending",
    EXPIRED = "Expired",
    LOCKED = "Locked",
    CREATED = "Created", // default value,
    DEACTIVATED = "Deactivated"
}
