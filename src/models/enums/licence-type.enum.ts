export enum LicenceType {
    PACKAGE = "Package",
    PAY_AS_YOU_GO = "Pay as You go"
}