export enum SubscriptionType {
    YEARLY = "Yearly",
    MONTHLY = "Monthly",
    LIMITLESS = "Limitless"
}