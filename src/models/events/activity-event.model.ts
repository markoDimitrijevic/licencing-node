import { ActivityType } from "../enums/activity-type.enum";

export interface ActivityEvent {
    customerId?: string,
    businessName: string,
    type: ActivityType,
    sessionId: string
}