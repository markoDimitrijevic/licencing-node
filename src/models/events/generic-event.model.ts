import { EventType } from "../enums/event-type.enum";
import { ActivityEvent } from "./activity-event.model";


export interface GenericEvent {
    type: EventType;
    message: string;
    eventObject: ActivityEvent
}