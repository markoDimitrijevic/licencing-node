import { Request, Response } from "express";
import { EntityManager, getManager } from "typeorm";
import { UpdateActivityRequestModel } from "../models/request-payloads/updateActivity.request";
import { ActivityRepository } from "../repositories/activity.repository";
import { PurgatoryRepository } from "../repositories/purgatory.repository";
import { sendResponse } from "../utils/wrappers/response-wrapper";

export class UpdateController {

    public static async updateActivity(request: Request, response: Response): Promise<any> {
        await getManager().transaction(async (entityManager: EntityManager) => {
            try {
                const body: UpdateActivityRequestModel = request.body
                const incompleteActivity = await ActivityRepository.findOne({ where: { customerId: null, sessionId: body.sessionId } });
                incompleteActivity ?
                    await ActivityRepository.update({ sessionId: body.sessionId }, { customerId: body.customerId }, entityManager) :
                    await PurgatoryRepository.insert({ sessionId: body.sessionId, customerId: body.customerId }, entityManager);
                sendResponse(response, 200, { body })
            } catch (error) {
                console.log(error.message);
                throw error
            }
        }).catch((error) => {
            console.log('The following error ocurred: ', error.message);
            sendResponse(response, 400, { message: error.message });
        });
    }
}