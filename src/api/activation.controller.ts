import { Request, Response } from "express";
import { EntityManager, getManager, In, Not } from "typeorm";
import { allowedLicenceStatuses, LicenceStatus } from "../models/enums/licence-status.enum";
import { allowedSubscriptionStatuses, SubscriptionStatus } from "../models/enums/subscription-status.enum";
import { ActivateLicenceRequestModel } from "../models/request-payloads/activateLicence.request";
import { ActivateSubscriptionRequestModel } from "../models/request-payloads/activateSubscription.request";
import { BusinessRepository } from "../repositories/business.repository";
import { LicenceRepository } from "../repositories/licence.repository";
import { SubscriptionRepository } from "../repositories/subscription.repository";
import { sendResponse } from "../utils/wrappers/response-wrapper";



export class ActivationController {
    public static async activateSubscription(request: Request, response: Response): Promise<any> {

        await getManager().transaction(async (entityManager: EntityManager) => {

            try {
                const body: ActivateSubscriptionRequestModel = request.body;

                // get all subscriptions of that business
                const business = await BusinessRepository.findOne({ where: { id: body.businessId } });
                if (!business) {
                    throw new Error('Invalid Business Id')
                }

                //check if subscriptionId is valid
                const newActiveSubscription = await SubscriptionRepository.findOne({ where: { id: body.subscriptionId, businessId: body.businessId } }, entityManager);
                if (!newActiveSubscription) {
                    throw new Error('Invalid Subscription Id')
                }

                // deactivate all 'Active' of them
                await SubscriptionRepository.update({ businessId: business.id, status: In([SubscriptionStatus.ACTIVE]), id: Not(body.subscriptionId) }, { status: SubscriptionStatus.DEACTIVATED }, entityManager);

                // activate the one sent in the request, if status is allowed
                await SubscriptionRepository.update({ id: body.subscriptionId, businessId: business.id, status: In(allowedSubscriptionStatuses) }, { status: SubscriptionStatus.ACTIVE }, entityManager);

                // set activeSubscriptionId on table Business
                await BusinessRepository.update({ id: body.businessId }, { activeSubscriptionId: body.subscriptionId });
                sendResponse(response, 200, { body });

            } catch (error) {
                console.log(error.message);
                throw error
            }
        })
            .catch((error) => {
                console.log('The following error ocurred: ', error.message);
                sendResponse(response, 400, { message: error.message });
            });
    }

    public static async activateLicence(request: Request, response: Response): Promise<any> {

        await getManager().transaction(async (entityManager: EntityManager) => {

            try {
                const body: ActivateLicenceRequestModel = request.body;

                // get all licences of that subscription
                const subscription = await SubscriptionRepository.findOne({ where: { id: body.subscriptionId } });
                if (!subscription) {

                    throw new Error('Invalid Subscription Id')
                }

                //check if LicenceId is valid
                const newActiveLicence = await LicenceRepository.findOne({ where: { id: body.licenceId, subscriptionId: body.subscriptionId } }, entityManager);
                if (!newActiveLicence) {
                    throw new Error('Invalid Licence Id')
                }

                // deactivate all 'Active' of them
                await LicenceRepository.update({ subscriptionId: subscription.id, status: In([LicenceStatus.ACTIVE]), id: Not(body.licenceId) }, { status: LicenceStatus.DEACTIVATED }, entityManager);

                // activate the one sent in the request, if status is allowed
                await LicenceRepository.update({ id: body.licenceId, subscriptionId: subscription.id, status: In(allowedLicenceStatuses) }, { status: LicenceStatus.ACTIVE }, entityManager);

                // set activeLicenceId on table Subscription
                await SubscriptionRepository.update({ id: body.subscriptionId }, { activeLicenceId: body.licenceId });
                sendResponse(response, 200, { body });

            } catch (error) {
                console.log(error.message);
                throw error
            }
        })
            .catch((error) => {
                console.log('The following error ocurred: ', error);
                sendResponse(response, 400, { message: error.message });
            });
    }
}