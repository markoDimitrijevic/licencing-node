import { Request, Response } from "express";
import { DataTypeNotSupportedError, TypeORMError } from "typeorm";
import { Activity } from "../entities/activity";
import { LicenceType } from "../models/enums/licence-type.enum";
import { SubscriptionStatus } from "../models/enums/subscription-status.enum";
import { AddNewBusinessRequestModel } from "../models/request-payloads/addNewBusiness.request";
import { AddNewLicenceRequestModel } from "../models/request-payloads/addNewLicence.request";
import { AddNewSubscriptionRequestModel } from "../models/request-payloads/addNewSubscription.request";
import { ActivityRepository } from "../repositories/activity.repository";
import { BusinessRepository } from "../repositories/business.repository";
import { LicenceRepository } from "../repositories/licence.repository";
import { SubscriptionRepository } from "../repositories/subscription.repository";
import { LittleHelpers } from "../utils/other/little-helpers";
import { sendResponse } from "../utils/wrappers/response-wrapper";


export class AdditionController {

    public static async addNewBusiness(request: Request, response: Response): Promise<any> {

        try {

            const body: AddNewBusinessRequestModel = request.body;
            await BusinessRepository.insert(body);
            sendResponse(response, 200, { body });

        } catch (error) {

            console.log(error.message);
            sendResponse(response, 400, { message: error.message });

        }

    }

    public static async addNewSubscription(request: Request, response: Response): Promise<any> {

        try {

            const body: AddNewSubscriptionRequestModel = request.body;
            await SubscriptionRepository.insert(body);
            sendResponse(response, 200, { body });

        } catch (error) {

            console.log(error.message);
            sendResponse(response, 400, { message: error.message });

        }

    }

    public static async addNewLicence(request: Request, response: Response): Promise<any> {

        try {

            const body: AddNewLicenceRequestModel = request.body;
            // extract maxNumberOfAllowedActivities and set it on licence
            const subscription = await SubscriptionRepository.findOneOrFail({ where: { id: body.subscriptionId } });

            if (subscription.status !== SubscriptionStatus.ACTIVE) {
                throw new Error('Cannot perform on status: ' + subscription.status + ". Status must be: Active")
            }

            let maxNumberOfAllowedActivities = 0, startDate: Date = null, endDate: Date = null
            switch (body.type) {
                case LicenceType.PACKAGE:
                    if (!body.startDate) {
                        startDate = subscription.activationDate;
                    }
                    endDate = subscription.expirationDate
                    maxNumberOfAllowedActivities = subscription.maxNumberOfAllowedActivities
                    break;
                case LicenceType.PAY_AS_YOU_GO:
                    if (!body.startDate) {
                        const activeLicence = await LicenceRepository.findOne({ where: { id: subscription.activeLicenceId } })
                        if (activeLicence) {
                            startDate = activeLicence.endDate
                        } else {
                            startDate = subscription.activationDate
                        }
                    }
                    endDate = null;
                    maxNumberOfAllowedActivities = null
                    break;
            }

            await LicenceRepository.insert({ ...body, maxNumberOfAllowedActivities, startDate, endDate });
            sendResponse(response, 200, { body });

        } catch (error) {

            console.log(error.message);
            sendResponse(response, 400, { message: error.message });

        }


    }

    public static async test(request: Request, response: Response): Promise<any> {

        try {

            const business = await BusinessRepository.findOneOrFail({ where: { businessName: request.body.businessName } });
            const activeSubscription = await SubscriptionRepository.findOneOrFail({ where: { id: business.activeSubscriptionId } });
            if (activeSubscription.status !== SubscriptionStatus.ACTIVE) {
                throw new Error('Cannot perform on status: ' + activeSubscription.status + ". Status must be: Active")
            }

            const activeLicence = await LicenceRepository.findOne({ where: { id: activeSubscription.activeLicenceId } });

            const lastActivity = await ActivityRepository.findOne({ where: { customerId: request.body.customerId, isBilled: true }, order: { date: "DESC" } });

            if (Object.values(activeSubscription.allowedActivities).includes(request.body.type)) {
                console.log('Da');
            } else {
                console.log('Ne');
            }
            const activityData = new Activity(activeLicence.id, request.body.type, request.body.sessionId, request.body.customerId ?? null)
            console.log(activityData);
            console.log(Date.now());

            console.log(typeof (LittleHelpers.formatDate(Date.now()).format()));


            if (!lastActivity) {
                console.log('No activity recorded');


            } else {
                const date3 = LittleHelpers.formatDate(Date.now()).format()
                const date2 = LittleHelpers.formatDate(lastActivity.date).format()
                const dateDifference2 = Math.abs(LittleHelpers.dateDifference(date2, date3))
                console.log(dateDifference2);


                if (dateDifference2 > activeSubscription.usagePeriod) {
                    console.log('Treba da se naplati');

                } else {
                    console.log("Ne treba da se naplati");

                }
                let isBilled = !lastActivity ? true : LittleHelpers.calculateIsBilled(lastActivity.date, activeSubscription);
                console.log(isBilled);

            }
            sendResponse(response, 200, { lastActivity });

        } catch (error) {

            console.log("Following error occured:", error.message);
            sendResponse(response, 400, { message: error.message });

        }

    }

}