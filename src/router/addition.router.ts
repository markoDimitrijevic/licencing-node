import { Router } from "express";
import { AdditionController } from "../api/addition.controller";

const router = Router();

router.post('/subscription', AdditionController.addNewSubscription);
router.post('/licence', AdditionController.addNewLicence);
router.post('/business', AdditionController.addNewBusiness);
router.post('/test', AdditionController.test);

export const AdditionRouter = router;