import { Router } from "express";
import { UpdateController } from "../api/update.controller";


const router = Router();

router.post('/activity', UpdateController.updateActivity);


export const UpdateRouter = router;