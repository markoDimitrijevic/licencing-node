import { Router } from "express";
import { ActivationController } from "../api/activation.controller";

const router = Router();

router.post('/subscription', ActivationController.activateSubscription);
router.post('/licence', ActivationController.activateLicence);

export const ActivationRouter = router;