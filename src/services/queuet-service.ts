import { QueueConfiguration, QueuetFactory, QueuetMessageQueue, QueueType } from "queuet";
import { EventType } from "../models/enums/event-type.enum";
import { GenericEvent } from "../models/events/generic-event.model";
import { validateQueuePayload } from "../utils/validation/validator";
import { env } from "../utils/wrappers/env-wrapper";
import { activityEventHandler } from "./queuet-handler";

export class _QueuetService {

    private readonly mq: QueuetMessageQueue;

    private handlerObject = {
        'Activity Event': activityEventHandler,
    }

    public constructor() {

        const queueConfig: QueueConfiguration = {
            vendor: (env.mq.vendor as QueueType),
            queueUrl: env.mq.url
        }

        switch (queueConfig.vendor) {
            case QueueType.AmazonSQS:
                queueConfig.aws = {
                    region: env.mq.awsRegion
                }
                break;
            case QueueType.RabbitMQ:
                queueConfig.rabbitMq = {
                    queueName: env.mq.rabbitName
                }
        }

        this.mq = QueuetFactory.getInstance(queueConfig);
    }

    public async registerEvents(): Promise<void> {
        setInterval(this.readMessages.bind(this), env.mq.readInterval);
    }

    private async readMessages(): Promise<void> {

        try {

            const messages = await this.mq.receiveMessage();
            if (messages.length === 0) return;

            for (const message of messages) {

                try {

                    const events: GenericEvent[] = JSON.parse(message.messageContent);

                    for (const event of events) {
                        /* Insert message handling logic here */
                        if (!(await validateQueuePayload(event))) continue;
                        console.log("Message:", event.message);
                        await this.handlerObject[event.type](event.eventObject as any);
                    }

                } catch (error) {
                    /* Insert error handling logic here */
                    console.log(error);
                }

            }

            await this.mq.deleteMessage(...messages);
        } catch (error) {
            /* Insert error handling logic here */
        }

    }

}

export const qs = new _QueuetService();
