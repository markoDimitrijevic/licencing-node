import { EntityManager, getManager } from "typeorm";
import { Activity } from "../entities/activity";
import { LicenceStatus } from "../models/enums/licence-status.enum";
import { SubscriptionStatus } from "../models/enums/subscription-status.enum";
import { ActivityEvent } from "../models/events/activity-event.model";
import { ActivityRepository } from "../repositories/activity.repository";
import { BusinessRepository } from "../repositories/business.repository";
import { LicenceRepository } from "../repositories/licence.repository";
import { PurgatoryRepository } from "../repositories/purgatory.repository";
import { SubscriptionRepository } from "../repositories/subscription.repository";
import { LittleHelpers } from "../utils/other/little-helpers";

export async function activityEventHandler(event: Partial<ActivityEvent>) {

    await getManager().transaction(async (entityManager: EntityManager) => {

        try {


            console.log(`Activity Event Handling...♨️`);

            const business = await BusinessRepository.findOneOrFail({ where: { businessName: event.businessName } })

            const activeSubscription = await SubscriptionRepository.findOneOrFail({ where: { id: business.activeSubscriptionId, status: SubscriptionStatus.ACTIVE } })
            if (!(Object.values(activeSubscription.allowedActivities).includes(event.type))) {
                throw new Error('Activity not allowed!')
            }

            const activeLicence = await LicenceRepository.findOneOrFail({ where: { id: activeSubscription.activeLicenceId, status: LicenceStatus.ACTIVE } });

            const lastActivity = await ActivityRepository.findOne({ where: { customerId: event.customerId, isBilled: true }, order: { date: "DESC" } });

            let isBilled = !lastActivity ? true : LittleHelpers.calculateIsBilled(lastActivity.date, activeSubscription);


            //check if customerId is sent in event.body, if not checks purgatory 
            if (!event.customerId) {
                const purgatoryData = await PurgatoryRepository.findOne({ where: { sessionId: event.sessionId } })
                event.customerId = purgatoryData ? purgatoryData.customerId : null
                if (purgatoryData) {
                    await PurgatoryRepository.delete({ where: { id: purgatoryData.id } }, entityManager)
                }
            }

            const activityData = new Activity(activeLicence.id, event.type, event.sessionId, isBilled, event.customerId)
            await ActivityRepository.insert(activityData, entityManager);

            if (activityData.isBilled) {
                await LicenceRepository.update({ id: activeSubscription.activeLicenceId, status: LicenceStatus.ACTIVE }, { numberOfSpentActivities: activeLicence.numberOfSpentActivities + 1 }, entityManager)
            }



            console.log("Activity Event Handled!♨️");

        } catch (error) {
            console.log(error.message);
            throw error
        }
    })
        .catch((error) => {
            console.log('The following error ocurred: ', error);

        });
}