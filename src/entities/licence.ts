import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from "typeorm";
import { Subscription } from "./subscription";
import { Activity } from "./activity"
import { LicenceStatus } from "../models/enums/licence-status.enum"
import { LicenceType } from "../models/enums/licence-type.enum";



@Entity()
export class Licence {
    @PrimaryGeneratedColumn()
    id!: number;

    @ManyToOne(() => Subscription, subscription => subscription.licences)
    subscription!: Subscription;

    @Column({ nullable: true, type: 'integer' })
    subscriptionId!: number;

    @Column({ nullable: true, type: 'integer', default: 0 })
    numberOfSpentActivities!: number;

    @Column({ nullable: true, type: 'integer' })
    maxNumberOfAllowedActivities!: number;

    @Column({ nullable: true, type: 'enum', enum: LicenceStatus, default: LicenceStatus.CREATED })
    status!: LicenceStatus; // default

    @Column({ nullable: true, type: 'timestamp' })
    startDate!: Date;

    @Column({ nullable: true, type: 'enum', enum: LicenceType })
    type!: LicenceType; // default

    @Column({ nullable: true, type: 'timestamp' })
    endDate!: Date;

    @OneToMany(() => Activity, activity => activity.licence)
    activities!: Activity[];

}

