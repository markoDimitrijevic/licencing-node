import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class AccountingPeriod {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ nullable: true, type: 'integer' })
    licenceId!: number;

    @Column({ nullable: true, type: 'integer' })
    numberOfNewCustomers!: number;

    @Column({ nullable: true, type: 'integer' })
    numberOfUpdates!: number;

    @Column({ nullable: true, type: 'integer' })
    numberOfFreeUpdates!: number;

    @Column({ nullable: true, type: 'integer' })
    totalAmountOfLicences!: number;

    @Column({ nullable: true, type: 'integer' })
    previousPeriodConsumption!: number;

    @Column({ nullable: true, type: 'integer' })
    totalLicenceConsumption!: number;

}
