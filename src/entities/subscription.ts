import { Business } from "./business";
import { Licence } from "./licence"
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from "typeorm";
import { SubscriptionType } from "../models/enums/subscription-type.enum";
import { SubscriptionStatus } from "../models/enums/subscription-status.enum";
import { ActivityType } from "../models/enums/activity-type.enum";




@Entity()
export class Subscription {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ nullable: true, type: 'enum', enum: SubscriptionStatus, default: SubscriptionStatus.CREATED })
    status!: SubscriptionStatus;

    @Column({ nullable: true, type: 'timestamp' })
    activationDate!: Date;

    @Column({ nullable: true, type: 'timestamp' })
    expirationDate!: Date;

    @Column({ nullable: true, type: 'enum', enum: SubscriptionType })
    type!: SubscriptionType;

    @Column({ nullable: true, type: 'integer' })
    usagePeriod!: number;

    @Column({ nullable: true, type: 'enum', enum: ActivityType, array: true })
    allowedActivities!: ActivityType[];

    @Column({ nullable: true, type: 'integer' })
    maxNumberOfAllowedActivities!: number;

    @Column({ nullable: true, type: 'character varying' })
    name!: string;

    @Column({ nullable: true, type: 'integer' })
    activeLicenceId!: number;

    @ManyToOne(() => Business, business => business.subscriptions)
    business!: Business;

    @Column({ nullable: true, type: 'integer' })
    businessId!: number;

    @OneToMany(() => Licence, licence => licence.subscription)
    licences!: Licence[];

}
