import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Licence } from "./licence"
import { ActivityType } from "../models/enums/activity-type.enum";
import { LittleHelpers } from "../utils/other/little-helpers";
import moment from "moment";

@Entity()
export class Activity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ nullable: true, type: 'character varying' })
    customerId!: string;

    @Column({ nullable: true, type: 'enum', enum: ActivityType })
    type!: ActivityType;

    @Column({ nullable: true, type: 'timestamp' })
    date!: Date;

    @Column({ nullable: true, type: 'boolean' })
    isBilled!: boolean;

    @Column({ nullable: true, type: 'character varying' })
    sessionId!: string;

    @ManyToOne(() => Licence, licence => licence.activities)
    licence!: Licence;

    @Column({ nullable: true, type: 'integer' })
    licenceId!: number;

    constructor(licenceId: number, type: ActivityType, sessionId: string, isBilled = false, customerId: string = null, date = new Date()) {
        this.licenceId = licenceId;
        this.date = date;
        this.type = type;
        this.sessionId = sessionId;
        this.customerId = customerId;
        this.isBilled = isBilled;
    }
}
