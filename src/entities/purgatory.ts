import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Purgatory {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ nullable: true, type: 'character varying' })
    sessionId!: string;

    @Column({ nullable: true, type: 'character varying' })
    customerId!: string;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
    createdAt!: Date;

}