import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Subscription } from "./subscription";

@Entity()
export class Business {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ nullable: false, type: 'character varying', unique: true })
    businessName!: string;

    @Column({ nullable: true, type: 'integer' })
    activeSubscriptionId!: number;

    @OneToMany(() => Subscription, subscription => subscription.business)
    subscriptions!: Subscription[];
}
