import Joi from "joi";
import { ActivityType } from "../../models/enums/activity-type.enum";
import { EventType } from "../../models/enums/event-type.enum";
import { LicenceType } from "../../models/enums/licence-type.enum";
import { SubscriptionType } from "../../models/enums/subscription-type.enum";

export const Schemes = {

    /* Events */

    "Activity Event": Joi.object({
        type: Joi.string().valid(...Object.values(EventType)).required(),
        message: Joi.string().required(),
        eventObject: Joi.object().keys({
            customerId: Joi.string().optional(),
            businessName: Joi.string().required(),
            type: Joi.string().valid(...Object.values(ActivityType)),
            sessionId: Joi.string().required()
        }).required()
    }),


    /* Routes */
    "/add/business": Joi.object({
        businessName: Joi.string().required(),
    }),

    "/add/subscription": Joi.object({
        businessId: Joi.number().required(),
        activationDate: Joi.date().required(),
        expirationDate: Joi.date().greater(Joi.ref('activationDate')).required(),
        type: Joi.string().valid(...Object.values(SubscriptionType)),
        usagePeriod: Joi.number().required(),
        allowedActivities: Joi.array().items(Joi.string().valid(...Object.values(ActivityType))),
        name: Joi.string().required(),
        maxNumberOfAllowedActivities: Joi.number().required()
    }),

    "/add/licence": Joi.object({
        subscriptionId: Joi.number().required(),
        startDate: Joi.date(),
        type: Joi.string().valid(...Object.values(LicenceType)),
    }),

    "/activate/subscription": Joi.object({
        subscriptionId: Joi.number().required(),
        businessId: Joi.number().required()
    }),

    "/activate/licence": Joi.object({
        licenceId: Joi.number().required(),
        subscriptionId: Joi.number().required()
    }),

    "/update/activity": Joi.object({
        sessionId: Joi.string().required(),
        customerId: Joi.string().required()
    })



}
