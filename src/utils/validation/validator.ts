import { NextFunction, Request, Response } from "express";
import { Schemes } from './validation-schemes';
import Joi, { ValidationError } from "joi";
import { sendResponse } from "../wrappers/response-wrapper";
import { GenericEvent } from "../../models/events/generic-event.model";

export async function validateRequestPayload(request: Request, response: Response, next: NextFunction) {

    try {

        const body = request.body;

        const key = request.url as keyof typeof Schemes;
        if (Schemes[key]) Joi.assert(body, Schemes[key], { abortEarly: true, allowUnknown: false });

        next();

    } catch (error) {
        sendResponse(response, 400, { message: error.details[0].message });
    }

}


export async function validateQueuePayload(event: GenericEvent) {

    try {

        console.log('Validating Event Schema');
        if (!event.type) throw new ValidationError('', ['Type field is missing.'], '');

        const key = event.type as keyof typeof Schemes;
        if (!Schemes[key]) throw new ValidationError('', ['Unknown value in type field'], '');

        Joi.assert(event, Schemes[key]);

        console.log('Event Schema Successfully Validated');
        return true;

    } catch (error) {
        console.log('Reports Node Validation Error: ', error.details[0].message);
        return false;
    }

}