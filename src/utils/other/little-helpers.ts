import moment from "moment";
import * as tz from 'moment-timezone';
import { Subscription } from "../../entities/subscription";
tz

export class LittleHelpers {
    static dateDifference(start: string, end: string) {
        moment.suppressDeprecationWarnings = true;
        return moment(new Date(end)).diff(moment(start), 'days');
    }

    static formatDate(date?: any) {
        moment.suppressDeprecationWarnings = true;
        return moment(date).tz('Europe/Belgrade');
    }

    static calculateIsBilled(lastActivityDate, activeSubscription: Subscription): boolean {
        const currentDate = LittleHelpers.formatDate(Date.now()).format()
        const dateDifference = LittleHelpers.dateDifference(lastActivityDate, currentDate)
        let isBilled = dateDifference > activeSubscription.usagePeriod;
        return isBilled
    }
}