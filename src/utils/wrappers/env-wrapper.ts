class EnvWrapper {

    private getProperty(property: string): string {
        return process.env[property.toUpperCase()] || process.env[property.toLowerCase()] || "";
    }

    private toNumber(value: string): number {
        return +value;
    }

    private toBoolean(value: string): boolean {
        return value.toLowerCase() === "true";
    }

    public port = this.getProperty("port");
    public env_type = this.getProperty("env_type");

    public mq = {
        readInterval: this.toNumber(this.getProperty("mq_read_interval")) * 1000,
        url: this.getProperty("mq_url"),
        vendor: this.getProperty("mq_vendor"),
        rabbitName: this.getProperty("rabbit_mq_name"),
        awsRegion: this.getProperty("aws_sqs_region")
    }

    public pg = {
        host: this.getProperty("pg_host"),
        port: this.toNumber(this.getProperty("pg_port")),
        username: this.getProperty("pg_username"),
        password: this.getProperty("pg_password"),
        database: this.getProperty("pg_db_name"),
    }

    public orm = {
        synchronize: this.toBoolean(this.getProperty("orm_synchronize")),
        logging: this.toBoolean(this.getProperty("orm_logging"))
    }

}

export const env = new EnvWrapper();
