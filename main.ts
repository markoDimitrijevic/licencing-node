import { config } from "dotenv";
config();

import { createConnection } from "typeorm";
import { createServer } from "http";
import { qs } from "./src/services/queuet-service";
import { env } from "./src/utils/wrappers/env-wrapper";
import app from "./src/app";

(async function main(): Promise<void> {

    try {
        qs.registerEvents();

        await createConnection();

        createServer(app).listen(env.port);

        console.log('Server up and running');
    } catch (error) {
        console.log(error.message);
        process.exit(-1);
    }

})();

